package database

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"

	"os"
)

func Conn() (db *sql.DB) {
	dbDriver := os.Getenv("DB_DRIVER")
	dbHost := os.Getenv("DB_HOST")
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	ConnectionString := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&collation=utf8mb4_unicode_ci&parseTime=true&multiStatements=true&timeout=30s", dbUser, dbPass, dbHost, dbName)

	db, err := sql.Open(dbDriver, ConnectionString)
	if err != nil {
		log.Println(err.Error())
	}
	log.Println("connection ok!")
	return db
}


