package employee

import (

	"DockerGoMysqlPhpmyadmin/api/domain"

	"context"

)

var e EmployeeRepository

func FetchAll(ctx context.Context) ([]domain.Employee, error){

	return e.FetchAll(ctx)

}

func FetchById(ctx context.Context, id int) (*domain.Employee, error){

	return e.FetchById(ctx, id)

}

