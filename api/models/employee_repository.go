package employee

import (
	"DockerGoMysqlPhpmyadmin/api/database"
	"DockerGoMysqlPhpmyadmin/api/domain"
	"context"
	"database/sql"
	"log"
)



type EmployeeRepository struct {
}

func (r *EmployeeRepository) FetchAll(ctx context.Context) ([]domain.Employee, error){
	db := database.Conn()
	selDB, err := db.Query("SELECT id,name,city FROM employees ORDER BY id DESC")
	if err != nil {
		log.Println("error: ", err)
	}
	emp := domain.Employee{}
	var res []domain.Employee
	for selDB.Next() {
		err = selDB.Scan(&emp.Id, &emp.Name, &emp.City)
		if err != nil {
			log.Println(err)
		}
		res = append(res, emp)
	}

	defer func(db *sql.DB) {
		err := db.Close()
		if err != nil {
			log.Println("error closing database")

		}
	}(db)

	return res, nil
}

func (r *EmployeeRepository) FetchById(ctx context.Context, id int) (*domain.Employee, error) {
	db := database.Conn()

	selDB, err := db.Query("SELECT * FROM employees WHERE id=?", id)
	if err != nil {
		panic(err.Error())
	}
	emp := &domain.Employee{}
	for selDB.Next() {
		err = selDB.Scan(&emp.Id, &emp.Name, &emp.City, &emp.CreatedAt)
		if err != nil {
			panic(err.Error())
		}

	}
	defer db.Close()

	return emp, nil
}