package handler

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)


type JsonResponse struct {
	// Campo reservado para adicionar alguma meta data para a API response
	Meta   interface{} `json:"meta"`
	Data   interface{} `json:"data"`
	Errors []ApiError  `json:"errors"`
}

type MetaResponse struct {
	Count int `json:"count"`
}

type MessageResponse struct {
	Message string `json:"message"`
}

type MetaPaginatedResponse struct {
	Count      int `json:"count"`
	Offset     int `json:"offset"`
	Limit      int `json:"limit"`
	TotalCount int `json:"total_count"`
}

type JsonErrorResponse struct {
	Error *ApiError `json:"error"`
}

type ApiError struct {
	Id     int    `json:"id"`
	Type   string `json:"type"`
	Title  string `json:"title"`
	Detail string `json:"detail"`
	Status int    `json:"status"`
}

func (r *ApiError) Error() string {
	return r.Detail
}

type EmptyDataResponse struct{}

func GetFromCache() error {
	return nil
}

func SetToCache() error {
	return nil
}

// SendJsonResponse converte uma struct para json e envia a resposta
func SendJsonResponse(w http.ResponseWriter, response interface{}, statusCode int) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	w.WriteHeader(statusCode)

	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Println(time.Now().Format(time.RFC3339), " | ", http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	} else {
		log.Println(time.Now().Format(time.RFC3339), " | ", statusCode)
	}
}

// SendBadRequestResponse envia uma resposta do tipo http.StatusBadRequest
func SendBadRequestResponse(w http.ResponseWriter, errors []ApiError) {
	errResponse := NewJsonResponse(nil, errors, MetaResponse{
		Count: len(errors),
	})

	SendJsonResponse(w, errResponse, http.StatusBadRequest)
}

// SendErrorResponse envia uma resposta de erro.
//
// Caso o erro seja do tipo ApiError retorna o status definido na struct. Caso contrário, envia uma resposta com status http.StatusInternalServerError
func SendErrorResponse(w http.ResponseWriter, err error) {
	var errors []ApiError
	var status int
	apiError, ok := err.(*ApiError)

	if ok {
		errors = []ApiError{*apiError}
		status = apiError.Status
	} else {
		errors = []ApiError{{
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}}
		status = http.StatusInternalServerError
	}

	errResponse := NewJsonResponse(nil, errors, MetaResponse{
		Count: len(errors),
	})

	SendJsonResponse(w, errResponse, status)
}

// NewJsonResponse cria uma struct padronizada para envio da resposta
func NewJsonResponse(data interface{}, errors []ApiError, meta interface{}) JsonResponse {
	if errors == nil {
		errors = []ApiError{}
	}

	if data == nil {
		data = []interface{}{}
	}

	response := JsonResponse{
		Data:   data,
		Meta:   meta,
		Errors: errors,
	}

	return response
}
