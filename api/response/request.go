package handler

import (
"encoding/json"
"net/http"
)

func DecodeBody(r *http.Request, decode interface{}, disallowUnknownFields bool) error {
	decoder := json.NewDecoder(r.Body)

	if disallowUnknownFields {
		decoder.DisallowUnknownFields()
	}

	err := decoder.Decode(&decode)

	if err != nil {
		return &ApiError{
			Status: http.StatusBadRequest,
			Title:  err.Error(),
		}
	}

	return nil
}
