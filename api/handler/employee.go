package handler

import (
	_ "DockerGoMysqlPhpmyadmin/api/database"
	_ "DockerGoMysqlPhpmyadmin/api/domain"
	"DockerGoMysqlPhpmyadmin/api/models"
	"DockerGoMysqlPhpmyadmin/api/response"
	_ "database/sql"
	"github.com/julienschmidt/httprouter"
	_ "log"
	"net/http"
	"strconv"
)

func FetchAll(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	data, err := employee.FetchAll(r.Context())

	if err != nil {
		errors := []handler.ApiError{{
			Status: 500,
			Title:  "Failed fetching employees",
			Type:   "employee",
			Detail: "erro..",
		}}

		errResponse := handler.NewJsonResponse(nil, errors, handler.MetaResponse{
			Count: len(errors),
		})

		handler.SendJsonResponse(w, errResponse, 500)
	} else {
		response := handler.NewJsonResponse(data, nil, handler.MetaResponse{
			Count: len(data),
		})

		handler.SendJsonResponse(w, response, http.StatusOK)
	}
}

func FetchById(w http.ResponseWriter, r *http.Request, params httprouter.Params) {

	id, err := strconv.ParseInt(params.ByName("id"),10 ,64)

	if err != nil {
		handler.SendErrorResponse(w, err)
	}

	data, err := employee.FetchById(r.Context(), int(id))

	if err != nil {
		handler.SendErrorResponse(w, err)
		return
	}


	response := handler.NewJsonResponse(data, nil, handler.MetaResponse{
		Count: 1,
	})

	handler.SendJsonResponse(w, response, http.StatusOK)


}



