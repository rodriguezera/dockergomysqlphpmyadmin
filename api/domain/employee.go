package domain

type Employees struct {
	Employee []Employee `json:"employees"`
}

type Employee struct {
	Id    int `json:"id"`
	Name  string `json:"name"`
	City string `json:"city"`
	CreatedAt string `json:"created_at,omitempty"`
}
