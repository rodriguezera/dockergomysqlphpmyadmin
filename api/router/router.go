package router

import (
	"DockerGoMysqlPhpmyadmin/api/handler"
	"github.com/julienschmidt/httprouter"
	"time"

	"log"
	"net/http"
)

func LoadRoutes() {

	router := httprouter.New()
	router.GET("/", handler.FetchAll)
	router.GET("/:id", handler.FetchById)

	go log.Println("It's Alright")
	defer log.Println("Finished.")

	server := &http.Server{
		Addr:         ":8080",
		Handler:      router,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	log.Fatal(server.ListenAndServe())

}