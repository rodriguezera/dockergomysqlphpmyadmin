buildup:
	docker-compose -f docker-compose.yaml up --build --force-recreate
up: 
	docker-compose up -d
down:
	docker-compose down
restart:
	docker-compose restart
prune: 
	docker-compose down -v
logs:
	docker-compose logs --tail=120 -f
logs-app:
	docker-compose logs --tail=120 -f app
logs-db:
	docker-compose logs --tail=120 -f database
	


				
