package main

import (
	"DockerGoMysqlPhpmyadmin/api/router"
	_ "github.com/go-sql-driver/mysql"
	"log"

	"github.com/joho/godotenv"
)

func checkError(err error) {
	if err != nil {
		log.Println(err)
	}
}

func main() {

	err := godotenv.Load()
	if err != nil {
		return
	}

	//db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/database")

	//mystring := make(map[string]string)
	//mystring["nome"] = "Amaury"
	//mystring["sobrenome"] = "Soares"
	//j, _ := json.Marshal(mystring)
	//log.Println(string(j))
	router.LoadRoutes()

}
