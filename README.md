# DockerGoMyslPhpmyadmin
Boilerplate para desenvolvimento em GO com Mysql, Redis e PhpMyadmin

## Configuração/instalação

- Clone o repositório utilize o comando "make up" para iniciar os containers
- Para visualizar os logs do sistema digite "make logs-app"

## Acesso ao PhpMyadmin
- Logo após a instação dos containers você pode acessar o phpmyadmin no endereço http://localhost:8090